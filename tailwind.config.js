module.exports = {
  mode: 'jit',
  purge: {
    content: [
      './resources/**/*.antlers.html',
      './resources/**/*.blade.php',
      './content/**/*.md'
    ]
  },
  important: true,
  theme: {
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px'
    },
    fontFamily: {
      primary: ['Quicksand', 'sans-serif']
    },
    extend: {
      width: {
        container: "1200px",
        C1:"calc(1/12*100% - (1 - 1/12)*64px)",
        C2:"calc(2/12*100% - (1 - 2/12)*64px)",
        C3:"calc(3/12*100% - (1 - 3/12)*64px)",
        C4:"calc(4/12*100% - (1 - 4/12)*64px)",
        C5:"calc(5/12*100% - (1 - 5/12)*64px)",
        C6:"calc(6/12*100% - (1 - 6/12)*64px)",
        C7:"calc(7/12*100% - (1 - 7/12)*64px)",
        C8:"calc(8/12*100% - (1 - 8/12)*64px)",
        C9:"calc(9/12*100% - (1 - 9/12)*64px)",
        C10:"calc(10/12*100% - (1 - 10/12)*64px)",
        C11:"calc(11/12*100% - (1 - 11/12)*64px)",
      },
      height: {
        vh: "80vh"
      },
      maxHeight: {
        vh: "80vh"
      },
      colors: {
        primary: "var(--colors_primary)",
        secondary: "var(--colors_secondary)",
      },
      backgroundImage: {
        'logo_back': "var(--logo_back)",
        'nav_back': "url('/assets/stijlen/nav.svg')",
       }
    }
  },
  variants: {},
  plugins: [
    require('@tailwindcss/aspect-ratio')
  ],
} 