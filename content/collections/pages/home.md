---
id: home
blueprint: pages
title: Home
template: home
featured_work:
  - 801c942f-203a-4481-aaba-aec5c72a725a
  - c347a48c-1c74-4e53-ba6c-c025af962e29
updated_by: f421a529-b2ef-43a7-a2cf-0f22e164405b
updated_at: 1640032801
intro: |-
  Houd je van handmade? (handgemaakt werk)

  Nou, da's leuk! Ik ook!

  Via dit adres kun je terecht voor schilderijen en posters, geboorte-, verjaardags- of trouwkaarten en misschien durf je een muurschildering aan?

  Met elke aankoop steunt u christencollectief (.nl) of Kies leven (vrij nieuwe landelijke prolife organisatie).

  Zie je een werk dat je interessant vindt? Vul even het formulier in, dan neem ik zo snel mogelijk contact op!

  Hartelijke groet,
  Tineke
heading: Welkom!
featured:
  - 7ba65b73-a399-4d0a-9193-9ea95fda6849
  - a5e35968-7c7f-480d-acb7-130412c2154a
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Welcome to your new Statamic website.'
---
